# Novel 1
    
## Chapter 1

The sun slowly rose over the vast green expanses that were spread far and wide, as far as the eye can see. There was nothing much to see, save a few trees and one lump in the ground, inside this lump, was a door, a big circle door with a big gold handle. The sun’s reflection on both the handle and the small circular window in the door, were enough to blind anyone (regardless of their height) with this rising sun.


This house belonged to a young elf, shorter than the normal elf you’d find in these parts, never 
